package com.example.automobileslist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class CarAdapter extends ArrayAdapter<Car> {
    private LayoutInflater inflater;
    private int layout;
    private List<Car> cars;
    private Context context;

    public CarAdapter(@NonNull Context context, int resource, List<Car> cars) {
        super(context, resource, cars);
        this.context = context;
        this.cars = cars;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(this.layout, parent, false);

        ImageView avatar = view.findViewById(R.id.carPicture);
        TextView priceKGS = view.findViewById(R.id.carSomPrice);
        TextView priceUSD = view.findViewById(R.id.carDollarPrice);
        TextView model = view.findViewById(R.id.carName);
        TextView firstDescription = view.findViewById(R.id.firstDescription);
        TextView secondDescription = view.findViewById(R.id.secondDescription);
        TextView thirdDescription = view.findViewById(R.id.thirdDescription);
        TextView carCity = view.findViewById(R.id.carCity);
        TextView timeLeft = view.findViewById(R.id.timeLeft);
        TextView commentsCount = view.findViewById(R.id.carCommentQuantity);
        TextView photosCount = view.findViewById(R.id.carPictureQuantity);

        Car car = cars.get(position);

        avatar.setImageResource(context.getResources().getIdentifier(car.getAvatar(), "drawable", context.getPackageName()));
        priceKGS.setText(String.format("%s сом", car.getPriceKGS()));
        priceUSD.setText(String.format("$ %s", car.getPriceUSD()));
        model.setText(car.getModel());
        firstDescription.setText(car.getFirstRowDescription());
        secondDescription.setText(car.getSecondRowDescription());
        thirdDescription.setText(car.getThirdRowDescription());
        carCity.setText(car.getCity());
        timeLeft.setText(car.getTimeLeft());
        commentsCount.setText(String.format("%s", car.getCommentsCount()));
        if (car.getCommentsCount() == 0){
            commentsCount.setVisibility(View.GONE);
        }
        photosCount.setText(String.format("%s", car.getPhotosCount()));
        if (car.getPhotosCount() == 0){
            photosCount.setVisibility(View.GONE);
        }
        return view;
    }
}
