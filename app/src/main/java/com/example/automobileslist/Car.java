package com.example.automobileslist;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Car {
    public String getAvatar() {
        return avatar;
    }

    private String avatar;
    private String model;
    private float priceKGS;
    private float priceUSD;
    private String color;
    private String firstRowDescription;
    private String secondRowDescription;
    private String thirdRowDescription;
    private String city;
    private String timeLeft;
    private int commentsCount;
    private int photosCount;

    public Car(String avatar, String model, float priceKGS, float priceUSD, String color, String firstRowDescription, String secondRowDescription, String thirdRowDescription, String city, String timeLeft, int commentsCount, int photosCount) {
        this.avatar = avatar;
        this.model = model;
        this.priceKGS = priceKGS;
        this.priceUSD = priceUSD;
        this.color = color;
        this.firstRowDescription = firstRowDescription;
        this.secondRowDescription = secondRowDescription;
        this.thirdRowDescription = thirdRowDescription;
        this.city = city;
        this.timeLeft = timeLeft;
        this.commentsCount = commentsCount;
        this.photosCount = photosCount;
    }

    private static Car generateCar() {
        String[] colors = new String[]{
                "белый", "черный", "желтый", "зеленый"
        };
        Random random = new Random();

        String avatar = "mazda";
        String model = String.format("Some Model %s", random.nextInt());
        float priceKGS = random.nextInt(5000000) + (float) 200000;
        float priceUSD = priceKGS / 74.15f;
        String color = colors[random.nextInt(colors.length)];
        String firstRowDescription = "седан, бензин";
        String secondRowDescription = "2001г., 1.8л., автомат";
        String thirdRowDescription = "руль справа";
        String city = "Бишкек";
        String timeLeft = "2 минуты назад";
        int commentsCount = random.nextInt(11);
        int photosCount = random.nextInt(11);
        return new Car(avatar, model, priceKGS, priceUSD, color, firstRowDescription, secondRowDescription, thirdRowDescription, city, timeLeft, commentsCount, photosCount);
    }

    public static List<Car> generateCarsAds() {
        List<Car> carsAds = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            carsAds.add(Car.generateCar());
        }
        return carsAds;

    }

    public String getModel() {
        return model;
    }

    public float getPriceKGS() {
        return priceKGS;
    }

    public float getPriceUSD() {
        return priceUSD;
    }

    public String getColor() {
        return color;
    }

    public String getFirstRowDescription() {
        return firstRowDescription;
    }

    public String getSecondRowDescription() {
        return secondRowDescription;
    }

    public String getThirdRowDescription() {
        return thirdRowDescription;
    }

    public String getCity() {
        return city;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public int getPhotosCount() {
        return photosCount;
    }
}
