package com.example.automobileslist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.widget.Toolbar;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Car> cars;
    private ListView carsList;
    private CarAdapter carAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cars = Car.generateCarsAds();

        carsList = findViewById(R.id.carList);

        carAdapter = new CarAdapter(this, R.layout.automobile_row, cars);
        carsList.setAdapter(carAdapter);


    }
}
